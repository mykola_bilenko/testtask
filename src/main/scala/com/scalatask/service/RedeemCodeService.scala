package com.scalatask.service

import spray.http.StatusCodes
import spray.routing._

trait RedeemCodeService extends HttpService {

  val myRoute = {
    path("redeemCode") {
      get {
        parameter('code) { code =>
          handleRequest(code)
        }
      }
    } ~
      (ctx => ctx.complete(StatusCodes.NotFound, "Not found error here"))
  }

  def handleRequest(code: String): Route = {
    //stub for now
    if (code == "jh4390hj9024hj9023jgsfhlns")
      complete {
        "The value for the code is $40"
      }
    else
      ctx => ctx.complete(StatusCodes.BadRequest, "This code is not correct")
  }

}
