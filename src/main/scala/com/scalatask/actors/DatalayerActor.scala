package com.scalatask.actors

import akka.actor.Actor
import com.scalatask.actors.DatalayerActor.RedeemCode
import com.scalatask.datalayer.CodesDao

object DatalayerActor {
  case class RedeemCode(code: String)
}

class DatalayerActor extends Actor {

  override def receive: Receive = {
    case RedeemCode(code) => sender ! CodesDao.redeemCode(code)
  }
}
