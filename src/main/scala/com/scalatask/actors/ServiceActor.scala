package com.scalatask.actors

import akka.actor.Actor
import com.scalatask.service.RedeemCodeService

class ServiceActor extends Actor with RedeemCodeService {

  def actorRefFactory = context

  def receive = runRoute(myRoute)
}


