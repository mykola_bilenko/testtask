package com.scalatask.datalayer

import slick.driver.PostgresDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object CodesDao {
  val db = Database.forConfig("postgres")

  val codes = TableQuery[Codes]

  def redeemCode(code: String) = {
    //TODO wrap in transaction
    lookupByCode(code) match {
      case (amount, false)::Nil => {
        updateExchangedOn(code)
        amount
      }
      case _ => new Exception("TODO return bad request correctly")
    }
  }

  def lookupByCode(code: String) = {
    val expr = codes.filter(x => x.code === code).map(x => (x.amount, x.exchanged))
    val action = expr.result
    val res = db.run(action)

    Await.result(res, Duration.Inf)
  }

  def updateExchangedOn(code: String) = {
    val expr = codes.filter(x => x.code === code).map(x => (x.timestamp, x.exchanged))
    val action  = expr.update(System.currentTimeMillis(), true)
    val res = db.run(action)

    Await.result(res, Duration.Inf)
  }

}
