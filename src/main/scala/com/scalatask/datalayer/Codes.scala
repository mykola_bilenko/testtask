package com.scalatask.datalayer

import slick.driver.PostgresDriver.api._

class Codes(tag: Tag) extends Table[(String, Double, Long, Boolean)](tag, "codes") {
  def code = column[String]("code", O.PrimaryKey)
  def amount = column[Double]("amount")
  def timestamp = column[Long]("timestamp")
  def exchanged = column[Boolean]("exchanged")

  def * = (code, amount, timestamp, exchanged)
}
