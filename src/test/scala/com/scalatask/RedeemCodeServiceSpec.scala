package com.scalatask

import com.scalatask.service.RedeemCodeService
import org.specs2.mutable.Specification
import spray.http.StatusCodes._
import spray.http._
import spray.testkit.Specs2RouteTest

class RedeemCodeServiceSpec extends Specification with Specs2RouteTest with RedeemCodeService {
  def actorRefFactory = system
  
  "MyService" should {

    "return an amount of money when requested correct code" in {
      Get("/redeemCode?code=jh4390hj9024hj9023jgsfhlns") ~> myRoute ~> check {
        responseAs[String] must contain("$40")
      }
    }

    "return a Bad Request error when requested code is incorrect" in {
      Get("/redeemCode?code=someIncorrectCode") ~> myRoute ~> check {
        status === BadRequest
        responseAs[String] === "This code is not correct"
      }
    }

    "return a 404 when something else requested" in {
      Get("/somehtingelse") ~> myRoute ~> check {
        status === NotFound
      }
    }
  }
}
