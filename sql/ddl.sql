CREATE TABLE codes (
    code varchar(30) NOT NULL,
    amount numeric NOT NULL,
    "timestamp" bigint,
    exchanged boolean DEFAULT false NOT NULL,
	PRIMARY KEY(code)
);
